# Magickal Servitors as a Tool for Mental Hygiene

In the world of psychotherapy there has been some research into assuaging feelings of guilt, shame, and worry. These feelings are all forms of anxiousness and when you are experiencing them your brain tries to make you feel better by attempting to release drugs into your system. Unfortunately when we are feeling down and the brain attempts to cheer us up and our reward centres are given a taste of the good stuff it's easy to then follow that behaviour with more of the same, compounding the issue. It is easy to become mired in these harmful behaviours. One way to deal with them is to instead find something to be grateful for. It doesn't matter what. Interestingly it isn't even important to remember an actual thing, just the act of remembering that you are grateful is enough to boost seratonin levels which gives your brain a little kick.

Magicians have long used servitors as a method for achieving results or making their will manifest in the world.

The methodology changes depending on the practitioner but the basic idea is this - define the idea, concentrate on giving it a form and purpose and then let them go. Think of them like subprocesses or daemons on a computer, which preform a single purpose or sit in the background performing useful tasks for you so you don't have to.

I see a great deal of overlap here. If you consider your anxious feelings as a minor demon and writing it down will allow you to name and therefore own that demon which can then be dealt with how you see fit. According to whichever school of magickal thought you subscribe to the creation and application of servitors differs so, in the words of academic mathematicians the world over, I leave that as an exercise for the reader.

Sources:
http://bigthink.com/robby-berman/4-things-you-can-do-to-cheer-up-according-to-neuroscience
http://www.philhine.org.uk/writings/sp_genpurpose.html
http://www.excommunicate.com/how-to-create-a-servitor/
